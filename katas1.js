function oneThroughTwenty() {
    const numbers = [];
    
    // Your code goes below
    for (let counter = 1; counter <=20; counter++){
        numbers.push(counter)
    }
    return numbers;
}
console.log(oneThroughTwenty())


function evensToTwenty() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 2; counter <=20; counter += 2){
        numbers.push(counter)
    }
    return numbers;
}
console.log(evensToTwenty())


function oddsToTwenty() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 1; counter <=20; counter += 2){
        numbers.push(counter)
    }
    return numbers;
}
console.log(oddsToTwenty())


function multiplesOfFive() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 1; counter <=20; counter++){
        numbers.push(counter * 5)
    }
    return numbers;
}
console.log(multiplesOfFive())


function squareNumbers() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 1; counter <=10; counter++){
        numbers.push(counter * counter)
    }
    return numbers;
}
console.log(squareNumbers())


function countingBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 20; counter >0; counter--){
        numbers.push(counter)
    }
    return numbers;
}
console.log(countingBackwards())


function evenNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 20; counter >0; counter -= 2){
        numbers.push(counter)
    }
    return numbers;
}
console.log(evenNumbersBackwards())


function oddNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 19; counter >=1; counter -= 2){
        numbers.push(counter)
    }
    return numbers;
}
console.log(oddNumbersBackwards())


function multiplesOfFiveBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 20; counter >0; counter--){
        numbers.push(counter * 5)
    }
    return numbers;
}
console.log(multiplesOfFiveBackwards())


function squareNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for (let counter = 10; counter >= 1; counter--){
        numbers.push(counter * counter)
    }
    return numbers;
}
console.log(squareNumbersBackwards())